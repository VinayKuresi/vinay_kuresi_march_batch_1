## YouTube Frontend Assignment.

#### *** Technologies Used : *** 

```
    1. HTML
    2. CSS
```

#### *** FrontPage Design. ***
![alternativetext](images/Main.png)

#### *** SideBar *** 
![alternativetext](images/SideBar.png)

#### *** Bottom Design. *** 
![alternativetext](images/Second.png)

#### *** Phone View. *** 
![alternativetext](images/PhoneView.png)

#### *** Botton View. *** 
![alternativetext](images/PhoneView2.png)