const express = require("express");
const PORT = 3000;
const app = express();
const path = require('path');

app.use('/', express.static(path.join(__dirname, 'public')))

app.get("/", (req, res) => {
    console.log(__dirname)
  res.sendFile(path.join(__dirname, 'public'));
});

app.listen(PORT, () => {
  console.log(`server running, http://localhost:${PORT}/`);
});
