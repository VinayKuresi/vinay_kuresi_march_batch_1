## YouTube adding dynamic functionality Assignment.
## Fetching API.


#### *** Technologies Used : *** 

```
    1. HTML
    2. CSS
    3. JavaScript.
    4. Server( Express).
    5. Github.
    5. Jsonplaceholder( Fake Online REST API for Testing and Prototyping).
```

#### *** Install Packages : ***

```
	Install the Packages : npm i
	Start Server : npm start
```

##### The Video details and related content are being fetched from the Json api hosted on Jsonplaceholder Json Server.

##### Assignment 5 work, installation of Node and packages, and working with server has done using this assignment.

#### *** FrontPage Design. ***
![picture](https://bitbucket.org/VinayKuresi/vinay_kuresi_march_batch_1/raw/1022a965a1c97a9f6e2d78d0ce01c6b0995ae8f1/Assignments/Assignment_4_5/images/front.PNG)


#### *** Onclick of Video in the Suggestion list, Rendering the Video *** 
![picture](https://bitbucket.org/VinayKuresi/vinay_kuresi_march_batch_1/raw/1022a965a1c97a9f6e2d78d0ce01c6b0995ae8f1/Assignments/Assignment_4_5/images/front_change.PNG)


#### *** Video comment. *** 
![picture](https://bitbucket.org/VinayKuresi/vinay_kuresi_march_batch_1/raw/1022a965a1c97a9f6e2d78d0ce01c6b0995ae8f1/Assignments/Assignment_4_5/images/comments.PNG)


#### *** POST success and response generated for the post comment.*** 
![picture](https://bitbucket.org/VinayKuresi/vinay_kuresi_march_batch_1/raw/1022a965a1c97a9f6e2d78d0ce01c6b0995ae8f1/Assignments/Assignment_4_5/images/response.PNG)   