// import fetch

async function getUserData() 
{
  let response = await fetch(`https://my-json-server.typicode.com/Vinaykuresi/type_code_json/data`);
  let data = await response.json()
  return data;
}

// search global variable
let searchText = "";
let previewList = [];

// parse data object get video id and title
const parseData = async () => {
  var data = await getUserData()
  var lt = data.map(o => {
    return {
      id: o.id,
      label: o.videoTitle
    };
  });
  return lt
}

  // suggestion list
const searchFilter = async(searchKey) => {
  let formattedList = await parseData();
  console.log(formattedList)
  if (searchKey) {
    previewList = formattedList.filter(
      o => o.label.toLowerCase().indexOf(searchKey) > -1
    );
  } else {
    previewList = [];
  }
   generateNewList();
};

const generateNewList = () => {
  document.querySelector(".search-list-container").innerHTML = "";
  if (previewList.length > 0) {
    const list = previewList.forEach(obj => {
      let node = document.createElement("li");
      node.className = "search-list-items";
      let textnode = document.createTextNode(obj.label);
      node.appendChild(textnode);
      document.querySelector(".search-list-container").appendChild(node);
    });
  }
};

document.querySelector("#search-input").addEventListener("keyup", (e) => {
   searchFilter(e.target.value);
});
document.querySelector("#search-input").addEventListener("focusin", e => {
  document.querySelector(".search-list").classList.remove("hidden");
});
document.querySelector("#search-input").addEventListener("focusout", e => {
  setTimeout(() => {
    document.querySelector(".search-list").classList.add("hidden");
    previewList = [];
  }, 1000);
});

// search functionality
document
  .querySelector(".search-list-container")
  .addEventListener("click", async(e) => {
    console.log("log: ", "clicked");
    const searchValue = e.target.innerHTML;
    searchText = searchValue;
    document.querySelector("#search-input").value = searchValue;
    var selectedItem =  await parseData()
    console.log(selectedItem[0])
    selectedItem = selectedItem.filter(i => i.label == searchValue);
    console.log(selectedItem)
    console.log("log: -- ", searchValue, selectedItem);
    if (selectedItem.length > 0) {
      loadNewVideo(selectedItem[0].id);
    }
  });

// main screen -- video view -- .current-play
let currentPlayingVideoId = "-EQO6YMIzqg";
const loadSelectedVideo = async() => {
  var data = await getUserData()
  const mainVideo = data.filter(i => i.id == currentPlayingVideoId)[0];
  document
    .querySelector(".current-play .main-video")
    .setAttribute("src", mainVideo.videoUrl);
  document.querySelector(".current-play .description .video-title").innerHTML =
    mainVideo.videoTitle;
  document.querySelector(
    ".current-play .description .review .views"
  ).innerHTML = mainVideo.totalViews;
  document.querySelector(".current-play .description .review #date").innerHTML =
    mainVideo.publishDate;

  document.querySelector(
    ".current-play .description .video-sentiments .like span"
  ).innerHTML = mainVideo.reviews.like;
  document.querySelector(
    ".current-play .description .video-sentiments .dislike span"
  ).innerHTML = mainVideo.reviews.dislike;

  // video comments
  let comments = mainVideo.comments.map(comment => {
    return `<div class="ph-5"><h4 class="user-name">${comment.userName}</h4>
    <h5 class="comment pv-5">${comment.comment}</h5>
    <div class="video-sentiments flex-a-c pv-5">
      <div class="like flex-a-c pr-5">
        <i class="fa fa-thumbs-up" for="up" aria-hidden="true"></i>
        <span class="ph-5">${comment.reviews.like }</span>
      </div>
      <div class="dislike flex-a-c">
        <i class="fa fa-thumbs-down" for="down" aria-hidden="true"></i>
        <span class="ph-5">${comment.reviews.dislike}</span>
      </div>
    </div>
    </div>`;
  });

  document.querySelector(".current-play .comment-list").innerHTML = comments;
};
loadSelectedVideo();

// Post a comment
document
  .querySelector(".comment .comment-submit-button")
  .addEventListener("click", e => {
    var data = document.querySelector(".comment .user-comment").value;
    console.log(data);
    fetch(
      `https://jsonplaceholder.typicode.com/posts`,
      {
        method: "POST",
        body: JSON.stringify({
          id: currentPlayingVideoId,
          usename: "Vinay Kumar Kuresi",
          comment: data
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
      }
    )
      .then(response => response.json())
      .then(json => {
        console.log(json);
      });
  });

// side videos preview
const refreshPlayList = async() => {
  var data = await getUserData()
  videoListWithoutCurrentPlayingVideo = data.filter(
    i => i.id !== currentPlayingVideoId
  );
  let videoList = videoListWithoutCurrentPlayingVideo.map(obj => {
    return `<div class="pv-5">
    <div onclick="onPlaylistVideoClick('${obj.id}')">
    <img
      height="200"
      width="400"
      class="main-video"
      src="${obj.videoThumbnail}"
    />
    </div>
    <div class="description">
      <h5>${obj.videoTitle}</h5>
    </div>
  </div>`;
  });
  document.querySelector(".right-side-video-list").innerHTML = videoList;
};

refreshPlayList();

const onPlaylistVideoClick = async(id) => {
  await loadNewVideo(id);
};

// load new video
const loadNewVideo = async(id) => {
  currentPlayingVideoId = id;
  console.log("log: current playing video id: ", currentPlayingVideoId);
  window.scroll({
    top: 0,
    left: 0,
    behavior: "smooth"
  });
  await loadSelectedVideo();
  await refreshPlayList();
};
               