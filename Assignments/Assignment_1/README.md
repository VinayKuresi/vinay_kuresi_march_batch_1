### Implementation PYTHON

### 1. HashMap

##### Dicts store an arbitrary number of objects, each identified by a unique dictionary key. 
##### Dictionaries are often also called maps, hashmaps, lookup tables, or associative arrays. 
##### They allow the efficient lookup, insertion, and deletion of any object associated with a given key.

##### The below is the scratch implementation of the HaspMap in Python. Code found in HashMao.py file.
##### To give a more practical explanation—phone books are a decent real-world analog for dictionaries.


    def _get_hash(self, key):
        pass
    
    def add(self, key, value):
        pass

    def get(self, key):
        pass


##### For most other UseCases, i have implemented various python built in implementation of HashMap.


    1. collections.OrderedDict – Remember the insertion order of keys
    2. collections.defaultdict – Return default values for missing keys
    3. collections.ChainMap – Search multiple dictionaries as a single mapping
    4. types.MappingProxyType – A wrapper for making read-only dictionaries
    5. Python Standard - built in Data Type

### 2. Array 
##### An array is a collection of items stored at contiguous memory locations. The idea is to store multiple items of the same type together. Array.py file contains Array data structure implementation.
##### Lists are just like the arrays, declared in other languages. Lists need not be homogeneous always which makes it a most powerful tool in Python. Array.py file contains List data structure implementation.

### 3. Set
##### Sets and their working
##### Set in Python can be defined as the collection of similar items. In Python, these are basically used to include membership testing and eliminating duplicate entries. The data structure used in this is Hashing, a popular technique to perform insertion, deletion and traversal in O(1) on average.
