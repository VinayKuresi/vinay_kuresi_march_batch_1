# Python program to demonstrate  
# Creation of Array  
  
# importing "array" for array creations 
import array as arr 
  
# creating an array with integer type 
a = arr.array('i', [1, 2, 3]) 
  
# printing original array 
print("The new created array is : ", end =" ") 
for i in range (0, 3): 
    print(a[i], end =" ") 
print() 
  
# creating an array with float type 
b = arr.array('d', [2.5, 3.2, 3.3]) 
  
# printing original array 
print("The new created array is : ", end =" ") 
for i in range (0, 3): 
    print(b[i], end =" ") 

#######################################################################

# Python program to demonstrate  
# Creation of List  
  
# Creating a List 
List = [] 
print("Intial blank List: ") 
print(List) 
  
# Creating a List with the use of a String 
List = ['LrnEd.io'] 
print("\nList with the use of String: ") 
print(List) 
  
# Creating a List with the use of multiple values 
List = ["Welcome", "to", "LrnEd.io"] 
print("\nList containing multiple values: ") 
print(List[0])  
print(List[2]) 
  
# Creating a Multi-Dimensional List 
# (By Nesting a list inside a List) 
List = [["Welcome", "to"] , ["LrnEd.io"]] 
print("\nMulti-Dimensional List: ") 
print(List) 