
# empty set, or use {} in creating set
x = set() 

# set {'e', 'h', 'l', 'o'} is created in unordered way
B = set('hello') 
print("B set : ", B)
# set{'a', 'c', 'd', 'b', 'e', 'f', 'g'} is created
A = set('abcdefg') 
print("A set : ", A)

# set{'a', 'b', 'h', 'c', 'd', 'e', 'f', 'g'} 
A.add('h')    
print("A set : ", A)

fruit ={'orange', 'banana', 'pear', 'apple'}
# fast membership testing in sets
result = 'pear' in fruit   
print("'pear' in fruit : ",result)

result = 'mango' in fruit
print("'mango' in fruit : ",result)

print("A is equivalent to B : ",A == B)

print("A is not equivalent to B : ",A != B)

print("A is subset of B A <B>= B : ",A <= B)   

print("A is proper superset of B : ",A > B)

print("the union of A and B : ",A | B) 

print("the intersection of A and B : ",A & B)

print("the set of elements in A but not B : ",A - B)

print("the symmetric difference : ",A^B)

a = {x for x in A if x not in 'abc'}   
print("Set Comprehension : ",a)