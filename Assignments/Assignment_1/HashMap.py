
# scratch Implementation of HashMap algorithm using a array data structure 

class HashMap:
    '''
    ClassDocs
    '''

    def __init__(self):
        '''
        constructor
        '''
        self.size = 64
        self.map = [None] * self.size

    # get the unique hash code implementation of the key
    def _get_hash(self, key):
        hash = 0

        for char in str(key):
            hash += ord(char)
        return hash % self.size

    # add a kay and value pair in the HashMap
    def add(self, key, value):
        key_hash = self._get_hash(key)
        key_value = [key, value]

        if self.map[key_hash] is None:
            self.map[key_hash] = list([key_value])
            return True
        else:
            for pair in self.map[key_hash]:
                if pair[0] == key:
                    pair[1] = value
                    return True
                else:
                    self.map[key_hash].append(list([key_value]))
                    return True

    # get the value of a particular key from the HashMap
    def get(self, key):
        key_hash = self._get_hash(key)
        if self.map[key_hash] is not None:
            for pair in self.map[key_hash]: 
                if pair[0] == key:
                    return pair[1]
        return None

    # delete a key and value pair from the HashMap
    def delete(self, key):
        key_hash = self._get_hash(key)
        del self.map[key_hash]

    # print all the HashMap key and value pairs
    def print_hashMap(self):

        print('---Phonebook---')
        for item in self.map:
            if item is not None:
                print(str(item))

if __name__ == "__main__":

    # create the HashMap object and storing the reference in a variable
    h = HashMap() 
   
    # add the key and value pairs to the HashMap
    h.add("name 1" , "9100000000")
    h.add("name 2" , "0581234323")
    h.add("name 3" , "9100004000")
    h.add("name 4" , "0681234323")

    # print all the hashMap objects
    h.print_hashMap()

    # get the value of a particular key
    print("\nprint the value of key 'name 2' : ",h.get("name 2"))

    # trying to add a duplicate key
    h.add("name 2" , "0581234555")

    # print all the hashMap objects associated with a given key
    h.print_hashMap()

    # print the hashcode of a key in HashMap
    print("\nprint the hash code value of a key : ",h._get_hash("name 1"))

    # delete a key and value pair from the HashMap
    h.delete("name 2")

    # print all the hashMap objects associated with a given key
    h.print_hashMap()

##################################################################

# Standard python built-in dictionary
# Declare a dictionary 
dict = {'Name': 'Zara', 'Age': 7, 'Class': 'First'}
# Accessing the dictionary with its key
print("dict['Name']: ", dict['Name'])
print("dict['Age']: ", dict['Age'])
dict['Age'] = 8; # update existing entry
dict['School'] = "DPS School"; # Add new entry
print("dict['Age']: ", dict['Age'])
print("dict['School']: ", dict['School'])
del dict['Name']; # remove entry with key 'Name'
dict.clear();     # remove all entries in dict
del dict ;        # delete entire dictionary
# print "dict['Age']: ", dict['Age'] # prints error --> TypeError: 'type' object is unsubscriptable

##################################################################

# collections.OrderedDict – Remember the insertion order of keys
import collections
d = collections.OrderedDict(one=1, two=2, three=3)
print(d)
d['four'] = 4
print(d)
print(d.keys())

##################################################################

# collections.defaultdict – Return default values for missing keys
from collections import defaultdict
dd = defaultdict(list)
# Accessing a missing key creates it and initializes it
# using the default factory, i.e. list() in this example:
dd['dogs'].append('Rufus')
dd['dogs'].append('Kathrin')
dd['dogs'].append('Mr Sniffles')
print(dd['dogs'])

##################################################################

# collections.ChainMap – Search multiple dictionaries as a single mapping
from collections import ChainMap
dict1 = {'one': 1, 'two': 2}
dict2 = {'three': 3, 'four': 4}
chain = ChainMap(dict1, dict2)
print(chain)
# prints --> ChainMap({'one': 1, 'two': 2}, {'three': 3, 'four': 4})
# ChainMap searches each collection in the chain
# from left to right until it finds the key (or fails):
print(chain['three'])
print(chain['one'])
#print(chain['missing']) #print --> KeyError: 'missing' 

##################################################################

# Types.MappingProxyType – A wrapper for making read-only dictionaries
from types import MappingProxyType
read_only = MappingProxyType({'one': 1, 'two': 2})
print(read_only['one'])
#print(read_only['one'] = 23) #TypeError: "'mappingproxy' object does not support item assignment"  